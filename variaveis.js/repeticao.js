let numero = 0;
console.log("REPETIÇÃO WHILE\n"); 
while(numero<=10){
    
    if(numero%2==0){
        console.log(`Valor Número: ${(numero)} é PAR`); 
    }else{
        console.log(`Valor Número: ${(numero)} é ÍMPAR`); 
    }
    numero ++;
}

/*---------------------------------------------*/

console.log("\nREPETIÇÃO DO/WHILE\n");
let numero1 = 0;
do{
    console.log(`Valor Nr: ${(numero1)}`);
    numero1++;
} while (numero1<=10);

/*---------------------------------------------*/

console.log("\nREPETIÇÃO FOR\n");
for(let numero2=0; numero2<=10;numero2++){
    console.log(`Valor Nrº: ${(numero2)}`);
}